﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFood : MonoBehaviour
{
    public GameObject foodPrefab;

    public Transform borderTop;
    public Transform borderBottom;
    public Transform borderLeft;
    public Transform borderRight;

    void Spawn()
    {
        // x position between left & right border
        int x = (int)Random.Range(-128,
                                  128);

        // y position between top & bottom border
        int y = (int)Random.Range(-128,
                                  128);

        // Instantiate the food at (x, y)
        Instantiate(foodPrefab,
                    new Vector2(x, y),
                    Quaternion.identity); // default rotation
    }


    // Start is called before the first frame update
    void Start()
    {
        // do a quick fill on the playing field
        for (int i = 0; i < 2000; i++)
        {
            Spawn();
        }
        for (int i = 0; i < 25; i++)
        {
            InvokeRepeating("Spawn", 1, 1);
        }

    }

    // Update is called once per frame
    void Update()
    {
    }

}
