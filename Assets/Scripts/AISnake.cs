﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AISnake : MonoBehaviour
{
    Vector2 dir = Vector2.right;
    // Did the snake eat something?
    bool ate = false;
    public float distancePerSecond;

    public GameObject foodPrefab;
    public GameObject tailPrefab;
    public GameObject AISnakePrefab;

    // Keep Track of Tail
    List<Transform> tail = new List<Transform>();

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Move", 0.2f, 0.2f);

    }

    // Update is called once per frame
    void Update()
    {
    }

    void Move()
    {
        // Save current position (gap will be here)
        Vector2 v = transform.position;

        Vector2 foodpos = FindClosestFood().gameObject.transform.position;

        float biggeststep = Mathf.Abs(0);
        float x = foodpos.x - v.x;
        float y = foodpos.y - v.y;

        if (x > 0)
        {
            if (dir != -Vector2.right) 
            { 
                if (biggeststep < Mathf.Abs(x))
                {
                    //Debug.Log("Left wins:" + Mathf.Abs(x));
                    dir = Vector2.right;
                    biggeststep = Mathf.Abs(x);
                }
            } 
        }

        if (x < 0)
        {
            if (dir != -Vector2.right)
            {
                if (biggeststep < Mathf.Abs(x))
                {
                    //Debug.Log("Right wins:" + Mathf.Abs(x));
                    dir = -Vector2.right;
                    biggeststep = Mathf.Abs(x);
                }
            }
        }

        if (y > 0)
        {
            if (dir != -Vector2.up)
            {
                if (biggeststep < Mathf.Abs(y))
                {
                    //Debug.Log("Up wins:" + y);
                    dir = Vector2.up;
                    biggeststep = y;
                }
            }
        }

        if (y < 0)
        {
            if (dir != Vector2.up)
            {
                if (biggeststep < Mathf.Abs(y))
                {
                    //Debug.Log("Down wins:" + y);
                    dir = -Vector2.up;
                    biggeststep = y;
                }
            }
        }


        //Debug.Log("x:" + x + " y:" + y + "  " + dir + " -- " + foodpos + " -- " + (foodpos - v) + " Winner is: " + biggeststep);

        // Move head into new direction (now there is a gap)
        transform.Translate(dir);

        // Ate something? Then insert new Element into gap
        if (ate)
        {
            // Load Prefab into the world
            GameObject g = (GameObject)Instantiate(tailPrefab,
                                                  v,
                                                  Quaternion.identity);

            // Keep track of it in our tail list
            tail.Insert(0, g.transform);

            // Reset the flag
            ate = false;
        }
        // Do we have a Tail?
        else if (tail.Count > 0)
        {
            // Move last Tail Element to where the Head was
            tail.Last().position = v;

            // Add to front of list, remove from the back
            tail.Insert(0, tail.Last());
            tail.RemoveAt(tail.Count - 1);
        }
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
        // Food?
        if (coll.name.StartsWith("food"))
        {
            // Get longer in next Move call
            ate = true;

            // Remove the Food
            Destroy(coll.gameObject);

        }
        else if (coll.name.StartsWith("TailPrefab"))
        {
            //destroy end of tail
            dir = -dir;
            transform.Translate(dir);

            int counter = tail.Count();
            for (int i = 0; i < counter ; i++)
            {
                Vector2 pos = tail.Last().position;
                Destroy(tail.Last().gameObject);
                tail.RemoveAt(tail.Count - 1);

                //add piece of food where tail was
                Instantiate(foodPrefab, new Vector2(pos.x, pos.y), Quaternion.identity); // default rotation
                Debug.Log("Count: " + tail.Count);
            }
            SpawnSnake();
            Destroy(gameObject);
        }
        else if (coll.name.StartsWith("AISnakePrefab"))
        {

        }
        else
        {
            Debug.Log("AI - Triggered Collider - " + coll.name);

        }
    }
    public GameObject FindClosestFood()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("foodPrefab");
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        return closest;
    }

    void SpawnSnake()
    {
        System.Random rnd = new System.Random();
        int x = rnd.Next(-128, 128);
        int y = rnd.Next(-128, 128);

        // load an AI Snake into the game
        Vector2 v = new Vector2((float)x, (float)y);
        // Load Prefab into the world
        GameObject g = (GameObject)Instantiate(AISnakePrefab, v, Quaternion.identity);
    }

}
