﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Startup : MonoBehaviour
{
    public GameObject foodPrefab;
    public GameObject AISnakePrefab;
    public Transform player;
    public float dampTime = 0.15f;
    private Vector3 velocity = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        System.Random rnd = new System.Random();
        for (int i = 0; i < 10; i++)
        {
            System.Threading.Thread.Sleep(50);
            SpawnSnake();
        }

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 point = GetComponent<Camera>().WorldToViewportPoint(player.position);
        Vector3 delta = player.position - GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
        Vector3 destination = transform.position + delta;
        transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
    }

    void SpawnSnake()
    {
        System.Random rnd = new System.Random();
        rnd.Next(8, 12);
        int x = rnd.Next(-128, 128);
        rnd.Next(8, 12);
        int y = rnd.Next(-128, 128);

        // load an AI Snake into the game
        Vector2 v = new Vector2((float)x, (float)y);
        // Load Prefab into the world
        GameObject g = (GameObject)Instantiate(AISnakePrefab, v, Quaternion.identity);
    }

}
