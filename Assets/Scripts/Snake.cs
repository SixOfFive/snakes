﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Snake : MonoBehaviour
{
    Vector2 dir = Vector2.right;
    // Did the snake eat something?
    bool ate = false;
    public float distancePerSecond;

    public GameObject foodPrefab;
    public GameObject tailPrefab;

    // Keep Track of Tail
    List<Transform> tail = new List<Transform>();

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Move", 0.1f, 0.1f);

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow) && dir != -Vector2.right)
            dir = Vector2.right;
        else if (Input.GetKey(KeyCode.DownArrow) && dir != Vector2.up)
            dir = -Vector2.up;    // '-up' means 'down'
        else if (Input.GetKey(KeyCode.LeftArrow) && dir != Vector2.right)
            dir = -Vector2.right; // '-right' means 'left'
        else if (Input.GetKey(KeyCode.UpArrow) && dir != -Vector2.up)
            dir = Vector2.up;

        if (Input.GetKey(KeyCode.D) && dir != -Vector2.right)
            dir = Vector2.right;
        else if (Input.GetKey(KeyCode.S) && dir != Vector2.up)
            dir = -Vector2.up;    // '-up' means 'down'
        else if (Input.GetKey(KeyCode.A) && dir != Vector2.right)
            dir = -Vector2.right; // '-right' means 'left'
        else if (Input.GetKey(KeyCode.W) && dir != -Vector2.up)
            dir = Vector2.up;

    }

    void Move()
    {
        Text onscreen;
        onscreen = GameObject.Find("Text").GetComponent<Text>();
        onscreen.text = tail.Count + " ";

        // Save current position (gap will be here)
        Vector2 v = transform.position;

        // Move head into new direction (now there is a gap)
        transform.Translate(dir);

        // Ate something? Then insert new Element into gap
        if (ate)
        {
            // Load Prefab into the world
            GameObject g = (GameObject)Instantiate(tailPrefab,
                                                  v,
                                                  Quaternion.identity);

            // Keep track of it in our tail list
            tail.Insert(0, g.transform);

            // Reset the flag
            ate = false;
        }
        // Do we have a Tail?
        else if (tail.Count > 0)
        {
            // Move last Tail Element to where the Head was
            tail.Last().position = v;

            // Add to front of list, remove from the back
            tail.Insert(0, tail.Last());
            tail.RemoveAt(tail.Count - 1);
        }
    }
    void OnTriggerEnter2D(Collider2D coll)
    {
        // Food?
        if (coll.name.StartsWith("food"))
        {
            // Get longer in next Move call
            ate = true;

            // Remove the Food
            Destroy(coll.gameObject);

        }
        // Collided with Tail or Border
        else if (coll.name.StartsWith("AISnakePrefab"))
        {
            //destroy end of tail
            dir = -dir;
            transform.Translate(dir);

            if (tail.Count > 0)
            {
                Destroy(tail.Last().gameObject);
                tail.RemoveAt(tail.Count - 1);

                //add piece of food where tail was
                Instantiate(foodPrefab,
                new Vector2(tail.Last().position.x, tail.Last().position.y),
                Quaternion.identity); // default rotation
            }

        }
        else if (coll.name.StartsWith("TailPrefab"))
        {
            //destroy end of tail
            dir = -dir;
            transform.Translate(dir);
            if (tail.Count > 0 )
            {

                //add piece of food where tail was
                Instantiate(foodPrefab,
                new Vector2(tail.Last().position.x, tail.Last().position.y),
                Quaternion.identity); // default rotation

                Destroy(tail.Last().gameObject);
                tail.RemoveAt(tail.Count - 1);
            }


        }
        else
        {
            Debug.Log("Triggered Collider - " + coll.name);

        }
    }
}
